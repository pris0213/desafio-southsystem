# Teste de API | Desafio de QA | South System
<!-- (descrição, configuração e execução) -->
Este é um projeto criado para o desafio de QA da South System.
O desafio consiste em criar testes para uma API pública.
A API escolhida para este projeto foi a [PokéAPI](https://pokeapi.co/).

## Requisitos
Este projeto foi desenvolvido em [Python](https://www.python.org/downloads/) (3.7) com [pipenv](https://github.com/pypa/pipenv). 
Para os testes de API com BDD, foi utilizado [behave](https://behave.readthedocs.io/en/latest/install.html), 
[requests](https://requests.readthedocs.io/en/master/) e [nose](https://nose.readthedocs.io/en/latest/). 

Para instalar as dependências, execute o comando abaixo na pasta do seu projeto:
```
$ pipenv install
```

## Testes
A PokéAPI é uma API apenas para o consumo de dados, ou seja, só aceita solicitações de `GET`.
Sendo assim, no arquivo `pokemon.feature`, adicionei a tag `@example` para os cenários que usam os métodos `POST`, 
`PUT` ou `DELETE`, indicando que são apenas exemplos. Estes cenários falham quando são rodados, justamente porque a
API é limitada apenas para consumo, não permitindo a modificação de seus dados.

### Execução dos Testes
Para executar os testes, vá até a pasta onde se encontram as features do projeto:
```
$ cd tests/integration/features/
```

Execute o comando:
```
$ behave
```

### Relatórios
Os relatórios de teste podem ser gerados através da ferramenta [Allure](https://github.com/allure-framework/allure-python). 
Para gerar os relatórios, siga os passos abaixo:
1. Baixe a ferramenta [Allure](https://docs.qameta.io/allure/#_installing_a_commandline);
2. Execute o comando abaixo para instalar o Allure para Behave:
    ```
    $ pip3 install allure-behave
    ```
3. Quando rodar os testes, não execute apenas o comando do Behave, mas sim este comando:
    ```
    $ behave -f allure_behave.formatter:AllureFormatter -o allure_reports
    ```
4. Finalmente, para visualizar os relatórios, execute o comando:
    ```
    $ allure serve allure_reports
    ```
