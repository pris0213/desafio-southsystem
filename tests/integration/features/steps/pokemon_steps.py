import requests
from nose.tools import assert_equals
from behave import given, when, then


pokemon_url = 'https://pokeapi.co/api/v2/'


@given('User sets API endpoint to get Pokémons')
def generate_get_endpoint(context):
    context.get_url = pokemon_url + 'pokemon/'


@when('User sends a request to get "{pokemon}"')
def get_pokemon_by_name(context, pokemon):
    pokemon = pokemon.lower()
    context.get_url = context.get_url + pokemon

    context.get_method_response = requests.get(context.get_url)


@then('The weight retrieved should be "{weight}"')
def assert_pokemon_weight(context, weight):
    weight = int(weight)
    response_json = context.get_method_response.json()
    response_weight = response_json['weight']

    assert_equals(response_weight, weight)


@given('User sets API endpoint for Pokémon colors')
def generate_post_endpoint_for_pokemon_colors(context):
    context.post_url = pokemon_url + 'pokemon-color/'


@when('User sends a request to add a new Pokémon color')
def post_pokemon_color(context):
    color = {
      "id": 11,
      "name": "jasmine white",
      "names": [
        {
          "name": "branco jasmim",
          "language": {
            "name": "pt-BR",
            "url": "https://pokeapi.co/api/v2/language/13/"
          }
        }
      ],
      "pokemon_species": [
        {
          "name": "floette",
          "url": "https://pokeapi.co/api/v2/pokemon-species/670/"
        }
      ]
    }

    context.post_method_response = requests.post(context.post_url, json=color)


@then('A successful status is returned for POST method')
def assert_post_response_is_successful(context):
    assert_equals(context.post_method_response.status_code, 201)


@given('User sets API endpoint for a specific Pokémon color')
def generate_endpoint_for_a_specific_color(context):
    context.method_url = pokemon_url + 'pokemon-color/' + '11'


@when('User sends a request to update the color')
def update_pokemon_color(context):
    color = {
        "id": 11,
        "name": "daisy white",
        "names": [
            {
                "name": "branco margarida",
                "language": {
                    "name": "pt-BR",
                    "url": "https://pokeapi.co/api/v2/language/13/"
                }
            }
        ],
        "pokemon_species": [
            {
                "name": "floette",
                "url": "https://pokeapi.co/api/v2/pokemon-species/670/"
            }
        ]
    }

    context.put_method_response = requests.put(context.method_url, json=color)


@then('A successful status is returned for PUT method')
def assert_put_response_is_successful(context):
    assert_equals(context.put_method_response.status_code, 200)


@when('User sends a request to delete the color')
def delete_pokemon_color(context):
    context.delete_method_response = requests.delete(context.method_url)


@then('A successful status is returned for DELETE method')
def assert_delete_response_is_successful(context):
    assert_equals(context.delete_method_response.status_code, 200)


@then('API should return an error for Page Not Found')
def assert_get_response_of_not_found(context):
    assert_equals(context.get_method_response.status_code, 404)


@when('User sends a request to add a new Pokémon color via XML')
def post_pokemon_color_via_xml(context):
    color = """<?xml version='1.0' encoding= 'UTF-8'?>
                <color>
                   <id>11</id>
                   <name>daisy white</name>
                   <names>
                      <element>
                         <language>
                            <name>pt-BR</name>
                            <url>https://pokeapi.co/api/v2/language/13/</url>
                         </language>
                         <name>branco margarida</name>
                      </element>
                   </names>
                   <pokemon_species>
                      <element>
                         <name>floette</name>
                         <url>https://pokeapi.co/api/v2/pokemon-species/670/</url>
                      </element>
                   </pokemon_species>
                </color>"""
    headers = {'Content-type': 'application/xml'}

    context.post_method_response = requests.post(context.post_url, headers=headers, data=color)


@then('API should return an error for Unsupported Media Type')
def assert_get_response_for_unsupported_media_type(context):
    assert_equals(context.post_method_response.status_code, 415)
