Feature: Pokémon API
  Testing GET, POST, PUT, DELETE methods on Pokémon API.

  @integration
  Scenario Outline: Retrieving Pokémon weight
    Given User sets API endpoint to get Pokémons
    When User sends a request to get "<pokemon>"
    Then The weight retrieved should be "<weight>"

    Examples:
    | pokemon | weight |
    | Snorlax | 4600   |

  @integration @example
  Scenario: Adding a new Pokémon color
    Given User sets API endpoint for Pokémon colors
    When User sends a request to add a new Pokémon color
    Then A successful status is returned for POST method

  @integration @example
  Scenario: Updating a color on Pokémon API
    Given User sets API endpoint for a specific Pokémon color
    When User sends a request to update the color
    Then A successful status is returned for PUT method

  @integration @example
  Scenario: Deleting a color on Pokémon API
    Given User sets API endpoint for a specific Pokémon color
    When User sends a request to delete the color
    Then A successful status is returned for DELETE method

  @integration @exception
  Scenario Outline: Retrieving a non-existent Pokémon
    Given User sets API endpoint to get Pokémons
    When User sends a request to get "<pokemon>"
    Then API should return an error for Page Not Found

    Examples:
    | pokemon |
    | Geromel |

  @integration @example @exception
  Scenario: Adding a new Pokémon color using XML
    Given User sets API endpoint for Pokémon colors
    When User sends a request to add a new Pokémon color via XML
    Then API should return an error for Unsupported Media Type